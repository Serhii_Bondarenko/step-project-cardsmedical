import Visit from "./visit.js";
import {urgencyOptions} from '../../constants/visitOptions.js';

export default class CardiologistVisit extends Visit {
    constructor({ id,
                    doctor = 'Cardiologist',
                    goal,
                    description,
                    urgency,
                    dateVisit,
                    fullName,
                    pressure,
                    bodyMass,
                    illness,
                    age
                }) {
        super({id, doctor, goal, description, urgency, dateVisit, fullName});
        this.pressure = pressure;
        this.bodyMass = bodyMass;
        this.illness = illness;
        this.age = age;
    }

    _renderMore() {
        super._renderMore();

        return `
            <div class="show-more d-none">
                     <div class="age mt-2">Вік: ${this.age}</div> 
                    <div class="goal mt-2">Мета візиту: ${this.goal}</div>
                    <div class="urgency mt-2">Терміновість: ${urgencyOptions[this.urgency]}</div>
                    <div class="desc mt-2 ">Детальніше: ${this.description}</div>           
                    <div class="pressure mt-2 ">Тиск: ${ this.pressure}</div>           
                    <div class="body-mass mt-2 ">Індекс маси: ${this.bodyMass}</div>           
                    <div class="illness mt-2 ">Перенесені серцеві захворювання: ${this.illness}</div>           
            </div>`;
    }
}